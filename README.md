# Meal Management

## Overview

The purpose of this system is to answer the age-old question: *What shall we
have for dinner?*

These scripts are used to maintain a SQLite database of meals, and to choose
one at random for a particular date. The date can be chosen explicitly or a
configuration file can define one day of the week to perform the choice.
Alternatively the choice will be made for the current day.

Meal details are held in a database. What is held is the name of the meal, the
minimal number of days between choices of that meal, when it was last eaten,
whether the meal is enabled or disabled at present, and a set of notes about
the meal. We don't store recipes, shopping lists or anything like that, we
just want to perform a choice such that we don't repeat the same meal too
often.

A log is held in the database which records the previous times a meal was
eaten. This is mainly for informational purposes.

This system is written for the Linux command line.

## Usage

There are two scripts that are used to choose the next meal and manage the
database. They are `choose_meal` and `manage_meal`.

### choose_meal

The basic function of this script is to choose a meal. This can be as simple
as choosing what to eat next Friday or choosing for a specific day each week.

Choosing for a particular day is achieved by giving the date via the
`-date=DATE` option. For example:

    ./choose_meal -date=2021-03-01

To test this you'd include the `-dry-run` option so a choice would be made but
not saved.

You might also use this mode to see what choices are available, then make an
example choice. Use the `-verbose` option for this:

    ./choose_meal -dry-run -verbose -date=2021-03-01

Without an explicit date the script uses a date computed from settings in its
configuration file. So, if you normally cook for friends or family on a
particular week day you can provide it there by name, such as `Wednesday`.
Then `choose_meal` will select a meal for the coming Wednesday if no specific
date is given.

If a meal has already been chosen for the selected or computed day the script
will report the fact and tell you what has been selected!

The `-help` option will cause the script to report information about all of
the options it accepts.

Hint: the script itself does not handle dates other than YYYY/MM/DD and
DD/MM/YYYY format but if you want to use dates like `'Tomorrow'` or `'next
Friday'` you can use the Linux `date` command like this:

```
$ ./choose_meal -date=$(date -d 'next Friday' +%F) -dry-run
Choice made for Friday 2021-03-05

Meal:             Haggis, Neeps & Tatties
Delay:            28
Last eaten on:    2021-01-27
Days since eaten: 37
Times eaten:      14
Choice not saved; in dry-run mode
```

#### Examples of use

The `choose_meal` script will give insight into what choices it might make
when you next run it. Try the following:

```
$ ./choose_meal -verb -dry
--------------------------------------------------------------------------------
Choosing from 4 meals:
- Vegetable and Lentil soup
- Ratatouille
- Vegetarian Bolognese
- Vegetarian Ragu
--------------------------------------------------------------------------------
Choice made for Wednesday 2021-03-31

Meal:             Vegetarian Bolognese
Delay:            28
Last eaten on:    2021-01-20
Days since eaten: 70
Times eaten:      13
Choice not saved; in dry-run mode
```
Here the next date is derived from the configuration file (the next
Wednesday). There are four meals that are eligible (enabled and with a
sufficient delay since they were last chosen). A random choice is made from
these and the details shown. Since `-dry-run` mode is on, nothing is saved.

* * *

The `-help` option lists all of the options and arguments of the `choose_meal`
script:

```
choose_meal version 0.1.8

Usage:
     choose_meal [-help] [-date=DATE] [-[no]dry-run] [-debug=N] [-[no]verbose] [-[no]colour]

Options:
    -help   Prints a brief help message describing the usage of the program,
            and then exits.

    -manpage
            Displays the entire documentation in manual page format.

    -date=DATE
            Optional date for the next meal choice. If omitted the next date
            is computed from the configuration file (if present), otherwise
            the current date is used.

    -[no]verbose
            Provide a little more information about choices (default off)

    -[no]dry-run
            Enable/disable dry run mode (default off)

    -[no]colour
            Enable/disable coloured text mode (default on)

    -debug=N
            Set the debug level

            0   No debug output, the default

            1   Displays the computation of the next target date from the
                current date

            2   As for 1 plus each database entry is reported including the
                date the meal was last eaten (after parsing) and the number
                of days since that date

            3   As for 2 plus the data read from the database is dumped as a
                Perl structure (hash of hashes indexed by the meal name)

    -config=FILE
            Define an alternative configuration file (default
            .choose_meal.yml in the same directory as the script)
```

### manage_meal

This script lets you add new meals to the database, change attributes of
existing meals and generate reports.

The `-help` option will cause the script to report information about all of
the options it accepts.

There is a `-dry-run` option which stops permanent changes from being made.

To add a new meal to the system use the `-add` option. The script will prompt
for the information it needs. If you use `dry-run` then nothing will actually
be written to the database (but some information about what would have been
done will be reported).

```
$ ./manage_meal -add -dry-run
Enter details of the new meal:
Name:          Brains on toast
Minimum delay: 365
Enabled:       n
Last eaten:
Notes:         This is disgusting
The following data would have been inserted:
SQL: INSERT INTO meal_history ( enabled, last_eaten, min_delay, name, notes) VALUES ( ?, ?, ?, ?, ? )
Bind values: 0,,365,Brains on toast,This is disgusting
```

Once a meal is in the system it can be modified.

The `-enable` and `disable` options control whether it can be chosen by
`choose_meal`. The minimum delay between being eligible for choice can be
changed with the `-delay=DAYS` option. The name can be changed with
`-edit=name` and the notes altered with `-edit=notes`. The editor invoked to
make these changes is Vim.

The meal name is required in order to make the change and case-insensitivity
can be selected with the `-ignore_case` option. The `-dry-run` option controls
whether the changes are actually made in the database.

If there are multiple matches to the meal name then these are listed to help
you be more precise in your name choice.

#### Examples of use

To list details of an existing meal in the system use the `-list` option. Add
`-full` to get the notes as well as the basic information. The name of the
meal is needed but this need not be the full name since the information
provided is taken to be a regular expression:

```
$ ./manage_meal -list Pie
Id:          10
Name:        Vegetarian Shepherd's Pie
Min delay:   56 days
Last eaten:  2021-01-13
Days since:  44 days
Times eaten: 11
Enabled:     Yes
Eligible:    No
--------------------------------------------------------------------------------
```

The name `'Pie'` is case-sensitive, so typing `'pie'` would fail. Use the
`-ignore-case` option to turn off case sensitivity.

If there are multiple matches to the meal name then these are listed to help
you be more precise in your name choice.

* * *

Another report is given with the `-eligible` option. The lists the meals which
are available for choice. This will be meals marked as 'enabled' which have
never been chosen before or where there has been a requisite number of days
since the last choice. The list is ordered alphabetically.

    ./manage_meal -eligible

The `-full` option here lists the notes.

It's also possible to determine what will be eligible on a future date by
adding the `-date=DATE` option.

    ./manage_meal -eli -date=$(date -d 'next wednesday' +%F)

* * *

The `-summary` option generates a summary of all of the meals in the system.
Their names are shown with the minimum delay between choices. The days since
the meal was last chosen is listed, with future choices having a negative
number. The report shows whether a meal is eligible for being chosen and
finally the meal's enabled state is shown. Enabled meals are in green, and
disabled ones in blue.

Meals stored in the system but not yet enabled (maybe awaiting a good recipe
or some practice in cooking the meal) are shown in this report.

* * *

The `-history` option reports on what meals were chosen when. It shows the
meal name, when eaten and whether it is enabled **now**. Some meals might
become unpopular or need to be unavailable for seasonal reasons or because
people are bored with them. This can be seen in this report. Enabled rows are
in green and disabled ones in blue.

This option takes a count if required: `-history=N`. This causes just the most
recent `N` history records to be shown.

* * *

The `-help` option lists all of the options and arguments of the `manage_meal`
script:

```
manage_meal version 0.1.8

Usage:
        manage_meal [-help] [-manpage] [-[no]dry-run] [-debug=N] [-[no]colour]
            [-[no]silent] [-[no]ignore-case] [-list] [-eligible] [-full]
            [-summary] [-history[=N]] [-date=DATE] [-enable] [-disable]
            [-last-eaten=DATE] [-edit=FIELD] [-delay=DAYS] [-add] [meal_name]

        manage_meal [-enable] [-disable] [-last-eaten=DATE] [-edit=FIELD]
             [-delay=DAYS] [-[no]dry-run] [-debug=N] [-[no]colour] [-[no]silent]
            [-[no]ignore-case] meal_name

        manage_meal -list [-full] [-[no]colour] [-[no]ignore-case] meal_name

        manage_meal -eligible [-full] [-[no]colour] [-date=DATE]

        manage_meal -summary [-[no]colour] [-date=DATE]

        manage_meal -history[=N] [-[no]colour]

        manage_meal -add [-[no]dry-run]

Options:
    -help   Prints a brief help message describing the usage of the program,
            and then exits.

    -manpage
            Displays the entire documentation in manual page format.

    -[no]dry-run
            Enables/disables dry run mode (default off). Only relevant to
            option combinations which can make changes to the database.

    -debug=N
            Set the debug level

            0:  no debug output

            1:  TBA

            2:  as for 1 plus the changes are shown before and after
                checking

            3:  as for 2 plus the data structures are dumped

    -[no]colour
            Enables/disables coloured text mode (default on, with colour)

    -[no]silent
            Enables/disables extra messages (default off, not silent)

    -[no]ignore-case
            Used with options that require a meal name list to make it
            case-insensitive (default off, case-sensitive)

    -list   Lists the chosen meal (without the notes) and exits

    -eligible
            Lists all eligible meals and exits

    -full   Shows the notes when listing

    -summary
            Summarises the state of all meals in the system

    -history[=N]
            Lists all or a subset of meals in the database and when they
            were eaten, in reverse chronological order, then exits

    -date=DATE
            Specifies the date for the eligibility test (and others)

    -enable Enables a meal option if not already enabled

    -disable
            Disables a meal option if not already disabled

    -last-eaten=DATE
            Sets the last_eaten date for a meal. Normally the choose_meal
            script does this, but after a new meal has been entered into the
            system after being eaten for the first time, the value can be
            added manually. This can be done at the time the meal details
            are added or later, using this option.

            This route cannot be used to change the date once a value
            exists. It is assumed that choose_meal looks after this
            information.

    -edit=FIELD
            Calls the Vim editor to allow FIELD to be edited. This applies
            to the 'name' and 'notes' fields only.

    -delay=DAYS
            Changes the number of days after eating the meal when it becomes
            eligible for choosing again. The smallest number you can use
            here is 14 days.

    -add    Add a new meal to the database. The required data is prompted
            for and written if it validates. Using -dry-run means the SQL
            and data are shown but no action is taken. The script exits
            after performing this function.

```

* * *

## Dependencies

The two scripts: `choose_meal` and `manage_meal` are written in Perl5. They
use a number of Perl modules which are listed here (and in the documentation).

### Perl Modules

```
DBI
DBD::SQLite
Data::Dumper
Date::Calc
Date::Parse
DateTime
File::Slurper
File::Temp
Getopt::Long
IO::Prompter
Pod::Usage
SQL::Abstract
Term::ANSIColor
YAML::XS
```

### Installing Perl Modules

1. First install the (Perl) tool `App::cpanminus`:\
   \
   `curl -L https://cpanmin.us | perl - --sudo App::cpanminus`\
   \
   This provides the command `cpanm`. This could be installed locally, but
   with `--sudo` it is installed at the system level.

2. Next, use `cpanm` to install the modules. One way of doing this is by using
   the file `modules_needed` included as part of the distribution:\
   \
   `cat modules_needed | xargs cpanm -S`\
   \
   This is a way of giving `cpanm` a list of all the modules the scripts need
   and asking it to install them using `sudo` to do so. On a system with no
   previous use of Perl it is likely that this process will take some time.

### Setting up the database

1. The scripts use a SQLite3 database so it is necessary for this package to
   have been installed. Doing this depends on the Linux version being used. On
   a Debian system installation can be achieved with this command:\
   \
   `sudo apt install sqlite3`

2. The database needs to be initialised with the `sqlite3` command. This can
   be achieved with the following command:\
   \
   `sqlite3 meals.db < meals_db.sql`\
   \
   The file `meals_db.sql` contains the necessary database definitions.


<!--
vim: syntax=markdown:ts=8:sw=4:ai:et:tw=78:fo=tcqn:fdm=marker
-->

