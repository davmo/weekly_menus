/* =============================================================================
 * Schema for meals.db
 *
 * Last change: 2022-07-23 22:26:15
 * =============================================================================
 */

/*
 * Table: meal_history
 *
 * id           primary key
 * name         name of menu item
 * min_delay    how many days to delay between repeats
 * last_eaten   date last eaten (date of Wednesday)
 * enabled      boolean true/false for whether this entry is to be selected
 * notes        somewhere to keep notes about this meal
 *
 */
DROP TABLE IF EXISTS meal_history;
CREATE TABLE IF NOT EXISTS meal_history (
    id                  integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    name                text NOT NULL,
    min_delay           integer NOT NULL,
    last_eaten          date,
    enabled             boolean NOT NULL DEFAULT 1,
    notes               text
);

/*
 * Unique constraint on the 'name' field
 */
 DROP INDEX IF EXISTS mh_name;
 CREATE UNIQUE INDEX mh_name ON meal_history(name);

/*
 * Table: meal_log
 *
 * id                   primary key
 * meal_history_id      foreign key of meal_history table
 * log_date             date added to the log
 * name                 name of menu item
 * min_delay            how many days to delay between repeats
 * last_eaten           date last eaten (date of Wednesday)
 *
 */
DROP TABLE IF EXISTS meal_log;
CREATE TABLE IF NOT EXISTS meal_log (
    id                  integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    meal_history_id     integer NOT NULL REFERENCES meal_history(id)
                                ON DELETE RESTRICT
                                ON UPDATE CASCADE,
    log_date            date NOT NULL default CURRENT_DATE,
    min_delay           integer NOT NULL,
    prev_eaten          date,
    last_eaten          date
);

/*
 * Trigger: log_trigger
 *
 * Whenever the 'last_eaten' field of 'meal_history' is changed we save details
 * into 'meal_log' We keep the name, delay, and the old and new values of
 * 'last_eaten' just so we could make some sort of frequency table from the data
 * if the mood ever took us.
 *
 */
DROP TRIGGER IF EXISTS log_trigger;
CREATE TRIGGER IF NOT EXISTS log_trigger AFTER UPDATE OF last_eaten ON meal_history
    BEGIN
        INSERT INTO meal_log (meal_history_id,min_delay,prev_eaten,last_eaten)
            VALUES (NEW.id, NEW.min_delay, OLD.last_eaten,NEW.last_eaten);
    END;

/*
 * View: history
 *
 * Similar to what's produced by './manage_meal -history'
 *
 * meal         meal name
 * eaten_on     date it was eaten
 * enabled      'Yes' or 'No'
 *
 */
DROP VIEW IF EXISTS history;
CREATE VIEW history AS
    SELECT
        mh.name as meal,
        ml.last_eaten as eaten_on,
        (CASE WHEN
            mh.enabled = 1
        THEN
            'Yes'
        ELSE
            'No'
        END) as enabled
    FROM meal_log ml
    JOIN meal_history mh ON mh.id = ml.meal_history_id
    ORDER BY ml.last_eaten DESC;


-- vim: syntax=sql:ts=8:sw=4:ai:tw=80:et:fo=tcrqn21:fdm=marker:comments+=b\:--
