#!/bin/bash -
#===============================================================================
#
#         FILE: cronjob
#
#        USAGE: ./cronjob
#
#  DESCRIPTION: Script to be run from cron for backing up the meals.db
#               database and tidying the old backups.
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Dave Morriss (djm), Dave.Morriss@gmail.com
#      VERSION: 0.0.5
#      CREATED: 2019-01-31 11:38:57
#     REVISION: 2021-07-02 21:54:38
#
#===============================================================================

set -o nounset                              # Treat unset variables as an error

SCRIPT=${0##*/}

BASEDIR="$HOME/Weekly_menus"
BACKUPDIR="$BASEDIR/backup"

#
# Create backup directory if not found
#
if [[ ! -d i$BACKUPDIR ]]; then
    mkdir "$BACKUPDIR" || { echo "Failed to created $BACKUPDIR"; exit 1; }
fi

#
# Make an array of months (make Bash do the arithmetic)
#
MAXMONTH=6
declare -a MONTHS
mapfile -t MONTHS < <(for (( i = 0; i <= MAXMONTH; i++ )); do echo $(( 28 * i )); done)

#
# Various variables
#
DB="$BASEDIR/meals.db"
BUP_PREFIX="meals_db"
BUP="$BACKUPDIR/${BUP_PREFIX}_$(date +"%Y%m%d_%H%M%S").sql"

#
# Sanity check
#
SQLITE3="$(command -v sqlite3)" || { echo "$SCRIPT: sqlite3 not found"; exit 1; }

#
# Snapshot the database and compress it
#
$SQLITE3 "$DB" .dump > "$BUP"
bzip2 "$BUP"

#
# Tidy database backup files
#
find "$BACKUPDIR" -type f -name "${BUP_PREFIX}_*.sql.bz2" -mtime +"${MONTHS[5]}" \
    -printf 'Deleted %f\n' -delete

exit

# vim: syntax=sh:ts=8:sw=4:ai:et:tw=78:fo=tcrqn21

